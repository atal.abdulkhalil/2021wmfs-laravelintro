@extends('layout')

@section('title', 'Overzicht')

@section('content')
    <div id="main">
        <!-- Event table -->
        <section id="event_table">
            <header class="major">
                <h2>Overzicht concerten</h2>
            </header>
            <form method="get" action="{{ url('/concerts') }}">
                <div class="row uniform 50%">
                    <div class="6u 12u$(xsmall)"></div>
                    <div class="3u 12u$(xsmall)">
                        <input type="text" name="search" id="search" value="{{ $term ?? '' }}" placeholder="Zoekterm" />
                    </div>
                    <div class="3u 12u$(xsmall)">
                        <input type="submit" value="Zoeken" class="special fit small" style="height: 3.4em"/>
                    </div>
                </div>
            </form>
            @if(!$concerts)
                <p>Geen concerten gevonden.</p>
            @else
            <div class="table-wrapper">
                <table>
                    <thead>
                    <tr>
                        <th>Datum</th>
                        <th>Naam en locatie</th>
                        <th>Prijs</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($concerts as $concert)
                        <tr>
                            <td>{{ date('d M y - H\ui', strtotime($concert->start_date)) }}</td>
                            <td><a href="{{ url('/concerts/' . $concert->id . '/details') }}"> {{ $concert->title }} ({{ $concert->location }}) @if($concert->fav) <a class="icon fa-star"/> @endif </a> <br/>
                                <form method="post" action="{{ url('/concerts/' . $concert->id . '/toggle') }}" style="margin: 0">
                                    <input type="hidden" name="event_id" value="{{ $concert->id }}" />
                                    <input type="hidden" name="moduleAction" value="switch" />
                                    <input type="submit" value="
                                    @if(!$concert->fav)
                                        voeg toe aan favorieten
                                    @else
                                        verwijder uit favorieten
                                    @endif" class="small" style="line-height:0em; height: 2em"/>
                                    @csrf
                                </form>
                            </td>
                            <td>{{ $concert->price }} &euro;</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>
            @endif
        </section>
    </div>
@endsection
