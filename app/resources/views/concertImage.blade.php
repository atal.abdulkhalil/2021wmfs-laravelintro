@extends('layout')

@section('title', $concert->title)

@section('content')

    <div id="main">
        <!-- Event table -->
        <section id="concert">
            <header class="major">
                <h2>{{ $concert->title }}</h2>
            </header>
            <div class="table-wrapper">
                <div class="box alt">

                    <div class="row 50% uniform">
                        <div class="12u$"><span class="image fit"><img src="{{ asset($concert->path) }}" alt="" /></span></div>
                    </div>
                </div>
                <p><a href="{{ url('/concerts/' . $concert->id . '/details') }}">Terug naar concert</a></p>
            </div>
        </section>
    </div>

@endsection
