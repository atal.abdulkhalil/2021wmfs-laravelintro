@extends('layout')

@section('title', $concert[0]->title)

@section('content')

    <!-- Main -->
    <div id="main">
        <!-- Event table -->
        <section id="concert">
            <header class="major">
                <h2>{{ $concert[0]->title }}</h2>
            </header>
            <div class="table-wrapper">
                <table>
                    <tbody>
                    <tr>
                        <th>Datum</th>
                        <td>{{ date('d M y - H\ui', strtotime($concert[0]->start_date)) }}</td>
                    </tr>
                    <tr>
                        <th>Locatie</th>
                        <td>{{ $concert[0]->location }}</td>
                    </tr>
                    <tr>
                        <th>Prijs</th>
                        <td>
                            {{ $concert[0]->price }} &euro;
                        </td>
                    </tr>
                    </tbody>
                </table>

                <div class="box alt">
                    <div class="row 50% uniform">
                        @foreach($concert as $item)
                            <div class="4u"><a href="{{ url('/concerts/' . $item->id . '/images/' . $item->image_id) }}" class="image fit thumb"><img src="{{ asset($item->path) }}" alt="" /></a></div>
                        @endforeach
                    </div>
                </div>
                <p><a href="{{ url('/concerts') }}">Terug naar overzicht</a></p>
            </div>
        </section>
    </div>

@endsection
