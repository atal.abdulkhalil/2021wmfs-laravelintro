<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('./concerts');
});

Route::get('/concerts', function (Request $request) {

    if ($request->has('search')) {
        return redirect('/search/' . urlencode($request->search));
    }

    $concerts = DB::select('select * from concerts');
    return view('concerts', ['concerts' => $concerts]);
});

Route::get('/search', function () {
    return redirect('/concerts');
});

Route::get('/search/{term}', function ($term) {
    $concerts = DB::select('select * from concerts where title like ?', ['%' . $term . '%']);
    return view('concerts', ['concerts' => $concerts, 'term' => $term]);
});

Route::post('/concerts/{id}/toggle', function ($id) {
    DB::statement('update concerts set fav = if(fav = 0,1,0) where id = ?', [$id]);
    return redirect('/');
});

Route::get('/concerts/{id}/details', function ($id) {
    $concert = DB::select('select concerts.id, title, location, price, start_date, images.id
                    as image_id, path from concerts left join images
                    on concerts.id = images.concert_id where concerts.id = ?', [$id]);
    return view('concertDetails', ['concert' => $concert]);
});

Route::get('/concerts/{id}/images/{imgId}', function ($id, $imgId) {
    $concert = DB::select('select concerts.id, title, path from concerts left join images
                    on concerts.id = images.concert_id where images.id = ?', [$imgId]);
    return view('concertImage', ['concert' => $concert[0]]);
});
